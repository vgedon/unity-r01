﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ToolTip : MonoBehaviour {
	public Text objectName;
	public Text objectDescr;
	[HideInInspector]public RectTransform rt;

	void Awake(){
		rt = GetComponent<RectTransform> ();
		DontDestroyOnLoad (this);
	}

	void Update (){
		if (rt == null)
			rt = GetComponent<RectTransform> ();
		if (!EventSystem.current.IsPointerOverGameObject ())
			gameObject.SetActive (false);
	}
}
