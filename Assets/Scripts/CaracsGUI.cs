﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CaracsGUI : MonoBehaviour {
	private Maya player;

	public Text labelName;

	public Text STR;
	public Text AGI;
	public Text CON;

	public Button STRPlus;
	public Button AGIPlus;
	public Button CONPlus;

	public Text armor;
	public Text minDmg;
	public Text maxDmg;

	public Text exp;
	public Text expLeft;
	public Text creds;

	public Text remPoints;

	// Use this for initialization
	void Start () {
		
		DontDestroyOnLoad (this);
	}
	
	// Update is called once per frame
	void Update () {
		if (player == null)
			player = Maya.maya;
		else {
			if (name != null)
				labelName.text = "Maya (lvl" + player.level + ")";
			if (STR != null)
				STR.text = "STR : " + player.STR;
			if (AGI != null)
				AGI.text = "AGI : " + player.AGI;
			if (CON != null)
				CON.text = "CON : " + player.CON;
			if (armor != null)
				armor.text = "Armor : " + player.Armor;
			if (minDmg != null)
				minDmg.text = "Min dmg : " + player.minDmg;
			if (maxDmg != null)
				maxDmg.text = "Max dmg : " + player.maxDmg;
			if (exp != null)
				exp.text = "Exp : " + player.xp;
			if (expLeft != null)
				expLeft.text = "Exp to next lvl : " + (player.nextLvl - player.xp);
			if (creds != null)
				creds.text = "Credits : " + player.money;
			if (player.Points > 0){
				if (STRPlus != null){
					STRPlus.interactable = true;
					STRPlus.gameObject.SetActive(true);
				}
				if (AGIPlus != null){
					AGIPlus.interactable = true;
					AGIPlus.gameObject.SetActive(true);
				}
				if (CONPlus != null){
					CONPlus.interactable = true;
					CONPlus.gameObject.SetActive(true);
				}
				if (remPoints != null){
					remPoints.gameObject.SetActive(true);
					remPoints.text = "Remaining points : " + player.Points;
				}
			}
			else{
				if (STRPlus != null){
					STRPlus.interactable = false;
					STRPlus.gameObject.SetActive(false);
				}
				if (AGIPlus != null){
					AGIPlus.interactable = false;
					AGIPlus.gameObject.SetActive(false);
				}
				if (CONPlus != null){
					CONPlus.interactable = false;
					CONPlus.gameObject.SetActive(false);
				}
				if (remPoints != null){
					remPoints.gameObject.SetActive(false);
					remPoints.text = "Remaining points : " + player.Points;
				}
			}
		}
	}
}
