﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class EndLevel : MonoBehaviour {

	public string nextLevel;
	private Scene scene;

	// Use this for initialization
	void Start () {
		scene = SceneManager.GetActiveScene ();
	}
	
	// Update is called once per frame
	void Update () {
	}

	void OnTriggerEnter(Collider coll) {
		Debug.Log ("change level");
		Maya.maya.GetComponent<UnityEngine.AI.NavMeshAgent> ().ResetPath ();
		if (scene.buildIndex < SceneManager.sceneCountInBuildSettings) {
			UnityEngine.SceneManagement.SceneManager.LoadScene (UnityEngine.SceneManagement.SceneManager.GetActiveScene ().buildIndex + 1);
		}
	}
}
