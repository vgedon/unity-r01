﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerGUI : MonoBehaviour
{
	private Maya player;
	public Image mayaHpsBar;
	public Text mayaHps;
	public Text mayaLvl;

	public Image xpBar;
	public Text xps;

	public Image enemyHp;
	public Text enemyName;

	public Button lvlup;

	public GameObject gameOver;
	public GameObject EnemyPanel;
	public GameObject stats;
	public GameObject inventory;

	public SkillButton skill1;
	public SkillButton skill2;
	public SkillButton skill3;
	public SkillButton skill4;


//	private Canvas thisCanvas;
	
	public static PlayerGUI gui;
	public ToolTip toolTip;

	// Use this for initialization
	void Start ()
	{
		player = Maya.maya;
//		thisCanvas = GetComponent<Canvas> ();
		gui = this;
		DontDestroyOnLoad (gameObject);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (player == null)
			player = Maya.maya;
		else {
			if (mayaLvl != null)
				mayaLvl.text = "Lvl " + player.level;
			if (mayaHps != null)
				mayaHps.text = player.hp.ToString ();
			if (mayaHpsBar != null) {
				Rect r = mayaHpsBar.rectTransform.rect;
				mayaHpsBar.rectTransform.localScale = new Vector3 (((float)(player.hp) / (float)(player.maxHp)), 1, 1);
			}
			if (xpBar != null) {
				Rect r = xpBar.rectTransform.rect;
				xpBar.rectTransform.localScale = new Vector3 (((float)player.xp / (float)player.nextLvl), 1, 1);
			}
			if (xps != null)
				xps.text = player.xp + " / " + player.nextLvl;
			if (player.target == null) {
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast (ray, out hit)) {
					if (hit.collider.tag == "Zombi") {
						ICharacter zomb = hit.collider.gameObject.GetComponent<ICharacter> ();
						if (zomb != null) {
							if (EnemyPanel != null)
								EnemyPanel.SetActive (true);
							if (enemyHp != null) {
								Rect r = enemyHp.rectTransform.rect;
								enemyHp.rectTransform.localScale = new Vector3 (((float)zomb.hp / (float)zomb.maxHp), 1, 1);
							}
							if (enemyName != null)
								enemyName.text = zomb.realName + " (lvl " + zomb.level + ")";
						}

					} else if (EnemyPanel != null)
						EnemyPanel.SetActive (false);
				}
			} else {
				if (EnemyPanel != null)
					EnemyPanel.SetActive (true);
				if (enemyHp != null) {
					Rect r = enemyHp.rectTransform.rect;
					enemyHp.rectTransform.localScale = new Vector3 (((float)player.target.hp / (float)player.target.maxHp), 1, 1);
				}
				if (enemyName != null)
					enemyName.text = player.target.name + " (lvl " + player.target.level + ")";
			}
			if (player.hp <= 0) {
				if (gameOver != null)
					gameOver.SetActive (true);
				if (stats != null)
					stats.SetActive (false);
			}
			if (player.Points > 0) {
				if (lvlup != null) {
					lvlup.interactable = true;
					lvlup.gameObject.SetActive (true);
				}
			} else {
				if (lvlup != null) {
					lvlup.interactable = false;
					lvlup.gameObject.SetActive (false);
				}
			}
			if (Input.GetKeyDown ("1") && skill1 != null)
				skill1.skillAction ();
			if (Input.GetKeyDown ("2") && skill2 != null)
				skill2.skillAction ();
			if (Input.GetKeyDown ("3") && skill3 != null)
				skill3.skillAction ();
			if (Input.GetKeyDown ("4") && skill4 != null)
				skill4.skillAction ();
		}
		if (Input.GetKeyDown ("n"))
			affStats ();
		if (Input.GetKeyDown ("i"))
			affInv ();
	}

	public void affInv ()
	{
		if (inventory != null)
			inventory.SetActive (!inventory.activeSelf);
	}

	public void affStats ()
	{
		if (stats != null)
			stats.SetActive (!stats.activeSelf);
	}

}
