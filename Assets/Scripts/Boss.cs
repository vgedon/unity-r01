﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Boss : ICharacter {
	private UnityEngine.AI.NavMeshAgent nma;
	private Animator anim;
	private bool run = false;
	private bool attack = false;
	private bool hide = false;
	private bool dead = false;
	private float lastAttack = 0;
	public ISkill skills;
	public GameObject exit;
	
	private int minDmg = 0;
	private int maxDmg = 0;
	
	// Use this for initialization
	void Start () {
		exit.SetActive(false);
		nma = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		anim = GetComponent<Animator> ();
		initStats ();
		hp = maxHp;
		skills = (ISkill)Instantiate(skills);
		setLvl (level);

	}
	
	// Update is called once per frame
	void Update () {
//		if (target == null) {
//			setLvl (Maya.maya.gameObject.GetComponent<Maya>().level);
//		}
		lastAttack -= Time.deltaTime;
		if (anim != null) {
			anim.SetBool("Run", run);
			anim.SetBool("Dead", hp <= 0);
			anim.SetBool("Attack", attack);
		}
		if (target != null && Vector3.Distance (transform.position, target.transform.position) < 1 && nma != null && hp > 0) {
			attack = true;
			run = false;
			nma.SetDestination(transform.position);
			transform.LookAt(target.transform.position);
		}
		if (target == null)
			attack = false;
		if (nma != null && !nma.hasPath)
			run = false;
		if (hp <= 0 && !dead) {
			StartCoroutine (hiding ());
			if (nma != null)
				nma.enabled = false;
			Maya.maya.addXp(20 + level * 10);
			exit.SetActive(true);
			dead = true;
			gameObject.AddComponent<RandomLootSystem>();
		}
		if (Maya.maya != null && Vector3.Distance (transform.position, Maya.maya.transform.position) < 10 
		    && Vector3.Distance (transform.position, Maya.maya.transform.position) > 1 && nma != null && hp > 0) {
			nma.SetDestination(Maya.maya.transform.position);
			run = true;
			attack = false;
			target = Maya.maya.gameObject.GetComponent<Maya>();
		}
		if (hide)
			transform.Translate (-transform.up * Time.deltaTime * 0.1f);
		if (attack && target != null && lastAttack <= 0 && !dead){
			transform.LookAt(target.transform.position);
			lastAttack = 1.5f;
			StartCoroutine(attacking(target));
		}
	}
	
	public override IEnumerator attacking(ICharacter target){
		yield return new WaitForSeconds (1f);
		if (target != null) {
			if (Random.Range (0, 2) == 0) {
				target.takeDamage(Random.Range(minDmg, maxDmg + 1), AGI);
			} else {
				skills.launcher = this;
				skills.launcher.target = Maya.maya;
				skills.action();
			}
		}
	}
	
	IEnumerator die(){
		yield return new WaitForSeconds (10);
		GameObject.Destroy (gameObject);
	}
	
	IEnumerator hiding(){
		yield return new WaitForSeconds(4.2f);
		hide = true;
		StartCoroutine(die());
	}

	public override void takeDamage(float damages, int oponentAGI){
		int chance = Random.Range (0, 100);
		if (chance < 75 + oponentAGI - AGI) {
			hp -= damages * (1 - (Armor / 200));
			Debug.Log ("GET HIT");
		}
	}
	
	private void initStats(){
		maxHp = CON * 10;
		minDmg = STR / 2;
		maxDmg = minDmg + 2;
	}

	public void setLvl(int lvl){
		float val;
		val = ((float)STR / 100f * 25f) * (lvl - 1);

		STR += (int)val;
		AGI += (int)val;
		CON += (int)val;
		initStats ();
		hp = maxHp;
		level = lvl;
	}
	
	public override void heal(int val){
		hp += val;
		if (hp > maxHp)
			hp = maxHp;
	}
}