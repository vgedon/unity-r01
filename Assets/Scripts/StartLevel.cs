﻿using UnityEngine;
using System.Collections;

public class StartLevel : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	void OnLevelWasLoaded() {
		Maya.maya.GetComponent<UnityEngine.AI.NavMeshAgent> ().Stop ();
		Maya.maya.GetComponent<UnityEngine.AI.NavMeshAgent> ().Warp (transform.position);
		Maya.maya.GetComponent<UnityEngine.AI.NavMeshAgent> ().Resume();

		Debug.Log (Maya.maya.GetComponent<UnityEngine.AI.NavMeshAgent> ().isOnNavMesh);
	}

	// Update is called once per frame
	void Update () {
	}
}