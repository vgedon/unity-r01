﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SkillTree : MonoBehaviour
{
	public List<ISkill> lvl1 = new List<ISkill> ();
	public List<ISkill> lvl2 = new List<ISkill> ();
	public List<ISkill> lvl3 = new List<ISkill> ();
	public List<ISkill> lvl4 = new List<ISkill> ();
	public List<ISkill> lvl5 = new List<ISkill> ();
	private List<Button> tbList = new List<Button> ();
	public Button buttonPrefab;

	private int spacement = 20;
	private int padding = 5;

	public Text talentPoints;

	// Use this for initialization
	void Start ()
	{
		DontDestroyOnLoad (this);
		if (lvl1 != null && lvl1.Count > 0) {
			GameObject panel = new GameObject ("Lvl1Skills");
			panel.AddComponent<CanvasRenderer> ();
			Image i = panel.AddComponent<Image> ();
			panel.transform.SetParent (gameObject.transform, false);
			i.color = Color.black;
			i.fillCenter = false;
			HorizontalLayoutGroup hlg = panel.AddComponent<HorizontalLayoutGroup> ();
			hlg.padding.left = padding;
			hlg.padding.right = padding;
			hlg.padding.top = padding;
			hlg.padding.bottom = padding;
			hlg.spacing = spacement;
			i.color = Color.clear;
			foreach (ISkill s in lvl1) {
				if (buttonPrefab != null && s != null) {
					Button but = (Button)Instantiate (buttonPrefab);
					if (but != null) {
						but.transform.SetParent (panel.transform);
						but.image.sprite = s.image;
						but.image.type = Image.Type.Simple;
						but.image.preserveAspect = true;
						TalentButton tb = but.GetComponent<TalentButton> ();
						if (tb != null) {
							tb.skillPrefab = s;
							tb.treelvl = 0;
						}
						tbList.Add (but);
					}
				}
			}
		}
		if (lvl2 != null && lvl2.Count > 0) {
			GameObject panel = new GameObject ("Lvl2Skills");
			panel.AddComponent<CanvasRenderer> ();
			Image i = panel.AddComponent<Image> ();
			panel.transform.SetParent (gameObject.transform, false);
			i.color = Color.black;
			i.fillCenter = false;
			HorizontalLayoutGroup hlg = panel.AddComponent<HorizontalLayoutGroup> ();
			hlg.padding.left = padding;
			hlg.padding.right = padding;
			hlg.padding.top = padding;
			hlg.padding.bottom = padding;
			hlg.spacing = spacement;
			i.color = Color.clear;
			foreach (ISkill s in lvl2) {
				if (buttonPrefab != null && s != null) {
					Button but = (Button)Instantiate (buttonPrefab);
					if (but != null) {
						but.transform.SetParent (panel.transform);
						but.image.sprite = s.image;
						but.image.type = Image.Type.Simple;
						but.image.preserveAspect = true;
						TalentButton tb = but.GetComponent<TalentButton> ();
						if (tb != null) {
							tb.skillPrefab = s;
							tb.treelvl = 1;
						}
						tbList.Add (but);
					}
				}
			}
		}
		if (lvl3 != null && lvl3.Count > 0) {
			GameObject panel = new GameObject ("Lvl3Skills");
			panel.AddComponent<CanvasRenderer> ();
			Image i = panel.AddComponent<Image> ();
			panel.transform.SetParent (gameObject.transform, false);
			i.color = Color.black;
			i.fillCenter = false;
			HorizontalLayoutGroup hlg = panel.AddComponent<HorizontalLayoutGroup> ();
			hlg.padding.left = padding;
			hlg.padding.right = padding;
			hlg.padding.top = padding;
			hlg.padding.bottom = padding;
			hlg.spacing = spacement;
			i.color = Color.clear;
			foreach (ISkill s in lvl3) {
				if (buttonPrefab != null && s != null) {
					Button but = (Button)Instantiate (buttonPrefab);
					if (but != null) {
						but.transform.SetParent (panel.transform);
						but.image.sprite = s.image;
						but.image.type = Image.Type.Simple;
						but.image.preserveAspect = true;
						TalentButton tb = but.GetComponent<TalentButton> ();
						if (tb != null) {
							tb.skillPrefab = s;
							tb.treelvl = 2;
						}
						tbList.Add (but);
					}
				}
			}
		}
		if (lvl4 != null && lvl4.Count > 0) {
			GameObject panel = new GameObject ("Lvl4Skills");
			panel.AddComponent<CanvasRenderer> ();
			Image i = panel.AddComponent<Image> ();
			panel.transform.SetParent (gameObject.transform, false);
			i.color = Color.black;
			i.fillCenter = false;
			HorizontalLayoutGroup hlg = panel.AddComponent<HorizontalLayoutGroup> ();
			hlg.padding.left = padding;
			hlg.padding.right = padding;
			hlg.padding.top = padding;
			hlg.padding.bottom = padding;
			hlg.spacing = spacement;
			i.color = Color.clear;
			foreach (ISkill s in lvl4) {
				if (buttonPrefab != null && s != null) {
					Button but = (Button)Instantiate (buttonPrefab);
					if (but != null) {
						but.transform.SetParent (panel.transform);
						but.image.sprite = s.image;
						but.image.type = Image.Type.Simple;
						but.image.preserveAspect = true;
						TalentButton tb = but.GetComponent<TalentButton> ();
						if (tb != null) {
							tb.skillPrefab = s;
							tb.treelvl = 3;
						}
						tbList.Add (but);
					}
				}
			}
		}
		if (lvl5 != null && lvl5.Count > 0) {
			GameObject panel = new GameObject ("Lvl5Skills");
			panel.AddComponent<CanvasRenderer> ();
			Image i = panel.AddComponent<Image> ();
			panel.transform.SetParent (gameObject.transform, false);
			i.color = Color.black;
			i.fillCenter = false;
			HorizontalLayoutGroup hlg = panel.AddComponent<HorizontalLayoutGroup> ();
			hlg.padding.left = padding;
			hlg.padding.right = padding;
			hlg.padding.top = padding;
			hlg.padding.bottom = padding;
			hlg.spacing = spacement;
			i.color = Color.clear;
			foreach (ISkill s in lvl5) {
				if (buttonPrefab != null && s != null) {
					Button but = (Button)Instantiate (buttonPrefab);
					if (but != null) {
						but.transform.SetParent (panel.transform);
						but.image.sprite = s.image;
						but.image.type = Image.Type.Simple;
						but.image.preserveAspect = true;
						TalentButton tb = but.GetComponent<TalentButton> ();
						if (tb != null) {
							tb.skillPrefab = s;
							tb.treelvl = 4;
						}
						tbList.Add (but);
					}
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (talentPoints != null)
			talentPoints.text = "Remaining points : " + Maya.maya.talentPoints;
	}
}