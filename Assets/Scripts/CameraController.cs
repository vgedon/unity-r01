﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	public GameObject Maya;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (this);
	}
	
	// Update is called once per frame
	void Update () {
		if (Maya != null){
			Vector3 pos = Maya.transform.position;
			pos.y += 7.5f;
			pos.z -= 7f;
			transform.position = pos;
		}
	}
}
