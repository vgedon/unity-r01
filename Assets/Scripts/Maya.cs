﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class Maya : ICharacter {
	[HideInInspector]public UnityEngine.AI.NavMeshAgent nma;
	private Animator anim;
	private bool run = false;
	private bool attack = false;
	private float lastAttack = 0;
	private bool locked = false;
	private GameObject weaponAnchor;

	private GameObject weapon;
	public ParticleSystem ps;
	public static Maya maya;
	public float minDmg = 0;
	public float maxDmg = 0;
	public float speed = 1f;
	public float player_range = 2f;
	public int xp = 0;
	public int money = 0;
	public int nextLvl = 100;

	public int Points = 0;
	public int talentPoints = 0;
	private float animSpeed;

	// Use this for initialization
	void Start () {
		nma = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		anim = GetComponent<Animator> ();
		animSpeed = anim.speed;
		if (maya == null)
			maya = this;
		initStats ();
		hp = maxHp;
		weaponAnchor = GameObject.FindGameObjectWithTag("WeaponAnchor");
		DontDestroyOnLoad (gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		initStats ();
		lastAttack -= Time.deltaTime;
		if (Input.GetMouseButton (0) && hp > 0 && EventSystem.current && !EventSystem.current.IsPointerOverGameObject()) {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit)) {
				if (hit.transform.tag == "Terrain" && nma != null && !locked) {
					if (nma.SetDestination (hit.point)) {
						run = true;
						attack = false;
					}
				}
				if (hit.transform.tag == "Zombi" && nma != null) {
					target = hit.collider.gameObject.GetComponent<ICharacter> ();
						if (Vector3.Distance (transform.position, target.transform.position) > get_weapon_range() && nma.SetDestination (hit.point)) {

						run = true;
						attack = false;
					}
					locked = true;
				}
				if (hit.transform.tag == "Weapon" && nma != null && !locked){
					if (Vector3.Distance (transform.position, hit.collider.transform.position) >= 2 && nma.SetDestination(hit.point)){
						run = true;
						attack = false;
					} else if (Vector3.Distance (transform.position, hit.collider.transform.position) < 2 && hit.collider.GetComponent<PickUpItem>()) {
						Item item = hit.collider.GetComponent<PickUpItem>().item;
						GetComponent<PlayerInventory>().inventory.GetComponent<Inventory>().addItemToInventory(item.itemID);
						Destroy (hit.collider.gameObject);
					}
				}
			}
		}
		if (Input.GetMouseButtonUp (0) || (target != null && target.hp <= 0)) {
			locked = false;
			target = null;
		}
		if (!nma.hasPath)
			run = false;
		if (anim != null) {
			if (attack == true){
				anim.speed = animSpeed * speed;
			}
			else
				anim.speed = animSpeed;
			anim.SetBool ("Run", run);
			anim.SetBool ("Dead", hp <= 0);
			anim.SetBool ("Attack", attack);
		}

		if (target != null && target.hp > 0 && Vector3.Distance(transform.position, target.transform.position) < get_weapon_range()){
			run = false;
			attack = true;
		}
		else 
			attack = false;	
		if (attack && target != null && lastAttack <= 0){
			transform.LookAt(target.transform.position);
			lastAttack = 0.75f;
			StartCoroutine(attacking(target));
		}
		if (hp < 0)
			hp = 0;
		if (hp > maxHp)
			hp = maxHp;
		if (xp >= nextLvl)
			newlvl ();
		if (Input.GetKeyDown ("l"))
			newlvl ();
	}

	public override IEnumerator attacking(ICharacter currtarget){
		yield return new WaitForSeconds (0.5f);
		if (currtarget != null) {
			currtarget.takeDamage (Random.Range (minDmg, maxDmg + 1), AGI);
		}
	}

	public override void takeDamage(float damages, int oponentAGI){
		int chance = Random.Range (0, 100);
		if (chance < 75 + oponentAGI - AGI)
			hp -= damages * (1 - (Armor/200));
	}

	public void SetWeapon (GameObject weapon) {
		GameObject w;
		if (!weapon.activeInHierarchy)
			w = Instantiate(weapon);
		else
			w = weapon;
		if (!w.Equals(this.weapon)){
			if (weaponAnchor.transform.childCount > 0)
				Destroy(weaponAnchor.transform.GetChild(0).gameObject);
			w.transform.SetParent(weaponAnchor.transform);
			w.transform.localPosition = Vector3.zero;
			w.transform.localScale = Vector3.one;
			w.GetComponent<Rigidbody>().isKinematic = true;
			w.transform.localEulerAngles = Vector3.zero;
			this.weapon = w;
			initStats();
		}
	}

	public float get_weapon_range()
	{
		if (player_range < 2.0f)
			return 2.0f;
		return player_range;
	}

	private void initStats(){
		float weaponDmg = 0;
		float armor = 0;
		player_range = 0;
		if (GameObject.FindGameObjectWithTag ("Player") != null && GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerInventory> () != null) {
			EquipmentSystem eS = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerInventory> ().characterSystem.GetComponent<EquipmentSystem> ();
			if (eS) {
//				ItemType[] itemTypeOfSlots = eS.itemTypeOfSlots;
				for (int i = 0; i < eS.slotsInTotal; i++) {
					if (eS.transform.GetChild (1).GetChild (i).childCount > 0) {
						GameObject item = eS.transform.GetChild (1).GetChild (i).GetChild (0).gameObject;
						List<ItemAttribute> attributs = item.GetComponent<ItemOnObject> ().item.itemAttributes;
						foreach (ItemAttribute att in attributs) {
							if (att.attributeName == "Damage")
								weaponDmg += att.attributeValue;
							if (att.attributeName == "Slots")
								speed = att.attributeValue;
							if (att.attributeName == "Armor")
								armor += att.attributeValue;
							if (att.attributeName == "Range")
								player_range += att.attributeValue;
						}
					}
				}
			}
		}
		armor_stuff = (int)armor;
		maxHp = CON * 5;
		minDmg = STR / 2 + weaponDmg;
		maxDmg = minDmg + 4;
		if (nextLvl == 0)
			nextLvl = 100;
	}

	public void addXp(int nb){
		xp += nb;
	}

	public void newlvl(){
		if (ps != null)
			ps.Play ();
		level++;
		initStats ();
		xp = xp - nextLvl;
		if (xp < 0)
			xp = 0;
		nextLvl += nextLvl / 2;
		hp = maxHp;
		Points += 5;
		talentPoints++;
	}

	public void remPoint(){
		Points--;
	}

	public void addSTR(){
		STR++;
		remPoint ();
	}

	public void addAGi(){
		AGI++;
		remPoint ();
	}

	public void addCON(){
		CON++;
		hp += 5;
		remPoint ();
	}

	void OnTriggerEnter(Collider col){
		if (col.tag == "LifeBubbule" && hp < maxHp) {
			hp += (int)((float)maxHp * 0.3f);
			GameObject.Destroy(col.gameObject);
		}
	}

	public override void heal(int val){
		hp += val;
		if (hp > maxHp)
			hp = maxHp;
	}
}
