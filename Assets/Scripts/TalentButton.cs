﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class TalentButton : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
	public ISkill skillPrefab;
	public ISkill theSkill;
	public Image butImage;
	public Text butTxt;
	public Button but;
	private RectTransform rt;
	public int treelvl;
	private bool toolTipAff = false;
	public SkillButton dependence;
	private GraphicRaycaster gr;
	private bool on_drag = false;

	// Use this for initialization
	void Start () {
		butImage = GetComponent<Image> ();
		butTxt = transform.GetComponentInChildren<Text> ();
		but = GetComponent<Button> ();
		if (skillPrefab != null) {
			theSkill = (ISkill)Instantiate (skillPrefab);
			theSkill.transform.SetParent(transform);
			theSkill.launcher = Maya.maya;
		}
		rt = GetComponent<RectTransform> ();
		gr = PlayerGUI.gui.GetComponent<GraphicRaycaster>();
		DontDestroyOnLoad (this);
	}
	
	// Update is called once per frame
	void Update () {
		if (theSkill == null && skillPrefab != null) {
			theSkill = (ISkill)Instantiate (skillPrefab);
			theSkill.transform.SetParent(transform);
			theSkill.launcher = Maya.maya;
		}
		if (theSkill != null) {
			if (butImage != null)
				butImage.sprite = theSkill.image;
			if (theSkill.cooldown > 0 && but != null) {
				but.interactable = false;
				if (butImage != null)
					butImage.color = Color.grey;
			} else if (but != null) {
				but.interactable = true;
				if (butImage != null)
					butImage.color = Color.white;
			}
		}

		if (treelvl <= Maya.maya.level / 6 && theSkill.lvl < theSkill.maxLvl) {
			but.interactable = Maya.maya.talentPoints > 0;
			butImage.color = Color.white;
		} else {
			but.interactable = false;
			butImage.color = Color.gray;
		}
		if (toolTipAff)
			ShowToolTip ();
	}

	public void skillUp(){
		if (theSkill != null) {
			theSkill.upgrade ();
			Maya.maya.talentPoints--;
		}
	}

	public void ShowToolTip(){
		toolTipAff = true;
		if (PlayerGUI.gui.toolTip != null && theSkill != null) {
			PlayerGUI.gui.toolTip.gameObject.SetActive(true);
			Vector3 pos = transform.position;
			pos.x += (PlayerGUI.gui.toolTip.rt.rect.width / 2) + (rt.rect.width / 2);
			PlayerGUI.gui.toolTip.transform.position = pos;
			PlayerGUI.gui.toolTip.objectName.text = theSkill.name.Replace("(Clone)", "") + "(lvl " + theSkill.lvl + ")";
			PlayerGUI.gui.toolTip.objectDescr.text = theSkill.ToString();
		}
	}
	
	public void HideToolTip(){
		toolTipAff = false;
		if (PlayerGUI.gui.toolTip != null)
			PlayerGUI.gui.toolTip.gameObject.SetActive (false);
	}


	//DRAGING
	public bool dragOnSurfaces = true;
	
	private GameObject m_DraggingIcon;
	private RectTransform m_DraggingPlane;

	public void OnBeginDrag(PointerEventData eventData)
	{
		if (!(treelvl <= Maya.maya.level / 6))
			return;
		Canvas canvas = FindInParents<Canvas>(gameObject);
		if (canvas == null)
			return;

		// We have clicked something that can be dragged.
		// What we want to do is create an icon for this.
		m_DraggingIcon = new GameObject("icon");
		
		m_DraggingIcon.transform.SetParent (canvas.transform, false);
		m_DraggingIcon.transform.SetAsLastSibling();

		Image image = m_DraggingIcon.AddComponent<Image>();
		
		image.sprite = GetComponent<Image>().sprite;
		image.rectTransform.sizeDelta = new Vector3(Camera.main.pixelHeight / 10, Camera.main.pixelHeight / 10);
		
		if (dragOnSurfaces)
			m_DraggingPlane = transform as RectTransform;
		else
			m_DraggingPlane = canvas.transform as RectTransform;
		
		SetDraggedPosition(eventData);
	}
	
	public void OnDrag(PointerEventData data)
	{
		if (!(treelvl <= Maya.maya.level / 6))
			return;
		on_drag = true;
		if (m_DraggingIcon != null)
			SetDraggedPosition(data);
	}
	
	private void SetDraggedPosition(PointerEventData data)
	{
		if (dragOnSurfaces && data.pointerEnter != null && data.pointerEnter.transform as RectTransform != null)
			m_DraggingPlane = data.pointerEnter.transform as RectTransform;

		RectTransform rt = m_DraggingIcon.GetComponent<RectTransform>();
		Vector3 globalMousePos;
		if (RectTransformUtility.ScreenPointToWorldPointInRectangle(m_DraggingPlane, data.position, data.pressEventCamera, out globalMousePos))
		{
			rt.position = globalMousePos;
			rt.rotation = m_DraggingPlane.rotation;
		}
	}
	
	public void OnEndDrag(PointerEventData eventData)
	{
		if (!on_drag)
			return;
		on_drag = false;
		List<RaycastResult> rr = new List<RaycastResult> ();
		gr.Raycast (eventData, rr);
		foreach (RaycastResult res in rr) {
			if (res.gameObject.tag == "Skill"){
				if (dependence != null){
					dependence.resetDependence();
					dependence = null;
				}
				SkillButton sb = res.gameObject.GetComponent<SkillButton>();
				dependence = sb;
				sb.skillPrefab = skillPrefab;
				sb.theSkill = theSkill;
				theSkill.transform.SetParent(dependence.transform);
				sb.dependence = this;
				sb.theSkill.launcher = Maya.maya;
			}
		}
		if (m_DraggingIcon != null)
			Destroy(m_DraggingIcon);
	}

	static public T FindInParents<T>(GameObject go) where T : Component
	{
		if (go == null) return null;
		T comp = go.GetComponent<T>();
		
		if (comp != null)
			return comp;
		
		Transform t = go.transform.parent;
		while (t != null && comp == null)
		{
			comp = t.gameObject.GetComponent<T>();
			t = t.parent;
		}
		return comp;
	}
}
