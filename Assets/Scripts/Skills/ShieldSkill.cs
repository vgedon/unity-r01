﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShieldSkill : ISkill {
	private int boostPerLvl = 20;
	private List<ParticleSystem> psInstance = new List<ParticleSystem>();
	private int armorBoost = 0;

	void Awake (){
		descr = "Reduces damages on the character for a short period.";
	}
	
	void Update(){
		if (remTime > 0)
			remTime -= Time.deltaTime;
		else
			remTime = 0;
		if (psInstance != null && psInstance.Count > 0) {
			for (int i = 0; i < psInstance.Count; i++)
				StartCoroutine(destroyPs(psInstance[i]));
		}
	}
	
	public override void action ()
	{
		if (remTime <= 0){
			armorBoost = boostPerLvl * lvl;
			launcher.ARMOR += armorBoost;
			remTime = cooldown;
			if (ps != null){
				Vector3 pos = launcher.transform.position;
				pos.y += 1.1f;
				ParticleSystem psi = (ParticleSystem)Instantiate(ps, pos, launcher.transform.rotation);
				psInstance.Add(psi);
				psi.transform.SetParent(launcher.transform);

			}
			StartCoroutine(subShield());
		}
	}
	
	public override void upgrade ()
	{
		lvl++;
		if (lvl > maxLvl)
			lvl = maxLvl;
	}
	
	public override string ToString ()
	{
		string ret = "Description : " + descr + "\r\n";
		ret += "Type : personnal.\r\n";
		ret += "Actual value : " + boostPerLvl * lvl + " armor.\r\n";
		ret += "Effective time : " + psTime + "s.\r\n";
		ret += "Cooldown : " + cooldown + "s.\r\n";
		if (lvl < maxLvl) {
			ret += "Next lvl : " + (boostPerLvl * lvl + 1) + " armor bonus.\r\n";
		}
		return ret;
	}
	
	IEnumerator destroyPs(ParticleSystem toDestroy){
		yield return new WaitForSeconds (psTime);
		if (toDestroy != null) {
			GameObject.Destroy(toDestroy.gameObject);
		}
	}

	IEnumerator subShield(){
		yield return new WaitForSeconds (psTime);
		launcher.ARMOR -= armorBoost;
	}
}
