﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnergyBolt : ISkill {
	private int boostPerLvl = 10;
	private List<ParticleSystem> psInstance = new List<ParticleSystem>();
	private int armorBoost = 0;

	void Awake (){
		descr = "Trhow an energy bolt on the enemy. Ignores armor.";
	}
	
	void Update(){
		if (remTime > 0)
			remTime -= Time.deltaTime;
		else
			remTime = 0;
		if (psInstance != null && psInstance.Count > 0) {
			for (int i = 0; i < psInstance.Count; i++)
				StartCoroutine(destroyPs(psInstance[i]));
		}
	}
	
	public override void action ()
	{
		if (remTime <= 0){
			remTime = cooldown;
			if (ps != null && launcher.target != null){
				Vector3 pos = launcher.target.transform.position;
				pos.y += 1.1f;
				psInstance.Add((ParticleSystem)Instantiate(ps, pos, launcher.target.transform.rotation));
				StartCoroutine(inflictsDmg());
			}
		}
	}
	
	public override void upgrade ()
	{
		lvl++;
		if (lvl > maxLvl)
			lvl = maxLvl;
	}
	
	public override string ToString ()
	{
		string ret = "Description : " + descr + "\r\n";
		ret += "Type : target.\r\n";
		ret += "Actual value : " + ((boostPerLvl * lvl) + launcher.AGI) + " damages.\r\n";
		ret += "Effective time : immediate.\r\n";
		ret += "Cooldown : " + cooldown + "s.\r\n";
		if (lvl < maxLvl) {
			ret += "Next lvl : " + ((boostPerLvl * (lvl + 1)) + launcher.AGI) + " damages.\r\n";
		}
		return ret;
	}
	
	IEnumerator destroyPs(ParticleSystem toDestroy){
		yield return new WaitForSeconds (psTime);
		if (toDestroy != null) {
			GameObject.Destroy(toDestroy.gameObject);
		}
	}

	IEnumerator inflictsDmg(){
		yield return new WaitForSeconds (1);
		if (launcher.target != null)
			launcher.target.hp -= (boostPerLvl * lvl) + launcher.AGI;
	}
}
