﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Berzerker : ISkill {
	private int boostPerLvl = 20;
	private List<ParticleSystem> psInstance = new List<ParticleSystem>();
	private int StrBoost = 0;

	void Awake (){
		descr = "Lose 10% HP to gin more STR.";
	}

	void Update(){
		if (remTime > 0)
			remTime -= Time.deltaTime;
		else
			remTime = 0;
		if (psInstance != null && psInstance.Count > 0) {
			for (int i = 0; i < psInstance.Count; i++)
				StartCoroutine(destroyPs(psInstance[i]));
		}
	}

	public override void action ()
	{
		if (remTime <= 0){
			StrBoost = boostPerLvl * lvl;
			launcher.STR += StrBoost;
			launcher.hp -= (int)(launcher.hp * 0.1f);
			remTime = cooldown;
			if (ps != null){
				Vector3 pos = launcher.transform.position;
				pos.y += 1.1f;
				ParticleSystem psi = (ParticleSystem)Instantiate(ps, pos, launcher.transform.rotation);
				psInstance.Add(psi);
				psi.transform.SetParent(launcher.transform);

			}
			StartCoroutine(subBerz());
		}
	}

	public override void upgrade ()
	{
		lvl++;
		if (lvl > maxLvl)
			lvl = maxLvl;
	}

	public override string ToString ()
	{
		string ret = "Description : " + descr + "\r\n";
		ret += "Type : personnal.\r\n";
		ret += "Actual value : " + boostPerLvl * lvl + " STR.\r\n";
		ret += "Effective time : " + psTime + "s.\r\n";
		ret += "Cooldown : " + cooldown + "s.\r\n";
		if (lvl < maxLvl) {
			ret += "Next lvl : " + (boostPerLvl * lvl + 1) + " STR bonus.\r\n";
		}
		return ret;
	}

	IEnumerator destroyPs(ParticleSystem toDestroy){
		yield return new WaitForSeconds (psTime);
		if (toDestroy != null) {
			GameObject.Destroy(toDestroy.gameObject);
		}
	}

	IEnumerator subBerz(){
		yield return new WaitForSeconds (psTime);
		launcher.STR -= StrBoost;
	}
}

