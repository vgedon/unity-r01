﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Grenade : ISkill {
	private int boostPerLvl = 10;
	private List<ParticleSystem> psInstance = new List<ParticleSystem>();
	public ParticleSystem areaPsPrefab;
	private bool targeting = false;
	private ParticleSystem areaPs;
	private Vector3 pos;

	void Awake (){
		descr = "Inflicts damages in a distant area after 2s. Ignores armor.";
	}
	
	void Update(){
		if (remTime > 0)
			remTime -= Time.deltaTime;
		else
			remTime = 0;
		if (psInstance != null && psInstance.Count > 0) {
			for (int i = 0; i < psInstance.Count; i++)
				StartCoroutine(destroyPs(psInstance[i]));
		}
		if (targeting && areaPs != null) {
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			RaycastHit[] hits;
			hits = Physics.RaycastAll (ray) ;
			if (hits != null){
				foreach (RaycastHit r in hits){
					if (r.transform.tag == "Terrain" && areaPsPrefab != null){
						areaPs.transform.position = r.point;
						pos = r.point;
					}
				}
			}
		}
		if (targeting && Input.GetMouseButtonDown (0)) {
			StartCoroutine(doExplosion(pos));
			targeting = false;
			Maya.maya.nma.ResetPath();
			if (areaPs != null)
				GameObject.Destroy(areaPs.gameObject);
			remTime = cooldown;
		}
		if (targeting && (Input.GetKeyDown ("escape") || Input.GetMouseButtonDown (1))) {
			targeting = false;
			Maya.maya.nma.ResetPath();
			if (areaPs != null)
				GameObject.Destroy(areaPs.gameObject);
		}
	}
	
	public override void action ()
	{
		if (remTime <= 0 && !targeting){
			if (ps != null){
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				RaycastHit hit;
				if (Physics.Raycast(ray, out hit)){
					if (hit.transform.tag == "Terrain" && areaPsPrefab != null)
						areaPs = (ParticleSystem)Instantiate(areaPsPrefab, hit.point, Quaternion.identity);
				}
				targeting = true;
			}
		}
	}
	
	public override void upgrade ()
	{
		lvl++;
		if (lvl > maxLvl)
			lvl = maxLvl;
	}
	
	public override string ToString ()
	{
		string ret = "Description : " + descr + "\r\n";
		ret += "Type : range / area(3m).\r\n";
		ret += "Actual value : " + ((boostPerLvl * lvl) + launcher.AGI) + " damges.\r\n";
		ret += "Effective time : 2s.\r\n";
		ret += "Cooldown : " + cooldown + "s.\r\n";
		if (lvl < maxLvl) {
			ret += "Next lvl : " + ((boostPerLvl * (lvl + 1)) + launcher.AGI) + " damages.\r\n";
		}
		return ret;
	}
	
	IEnumerator destroyPs(ParticleSystem toDestroy){
		yield return new WaitForSeconds (psTime);
		if (toDestroy != null) {
			GameObject.Destroy(toDestroy.gameObject);
		}
	}

	IEnumerator doExplosion(Vector3 vect){
		yield return new WaitForSeconds (2);
		if (ps != null) {
			psInstance.Add((ParticleSystem)Instantiate(ps, vect, Quaternion.identity));
		}
		RaycastHit[] hits;
		hits = Physics.SphereCastAll(vect, 3f, Vector3.up, 1);
		if (hits != null) {
			foreach (RaycastHit r in hits){
				if (r.transform.tag == "Zombi"){
					Zombi enemy = r.transform.GetComponent<Zombi>();
					if (enemy != null){
						enemy.hp -= (boostPerLvl * lvl) + launcher.AGI;
					}
				}
			}
		}
	}
}
