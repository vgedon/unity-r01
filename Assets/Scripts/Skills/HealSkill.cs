﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HealSkill : ISkill {
	private int boostPerLvl = 5;
	private List<ParticleSystem> psInstance = new List<ParticleSystem>();

	void Awake (){
		descr = "Heals the character.";
	}

	void Update(){
		if (remTime > 0)
			remTime -= Time.deltaTime;
		else
			remTime = 0;
		if (psInstance != null && psInstance.Count > 0) {
			for (int i = 0; i < psInstance.Count; i++)
				StartCoroutine(destroyPs(psInstance[i]));
		}
	}

	public override void action ()
	{
		if (remTime <= 0){
			float val = ((float)launcher.maxHp / 100f) * ((float)boostPerLvl * (float)lvl);
			launcher.heal ((int)val);
			remTime = cooldown;
			if (ps != null){
				Vector3 pos = launcher.transform.position;
				pos.y += 1.1f;
				psInstance.Add((ParticleSystem)Instantiate(ps, pos, launcher.transform.rotation));
			}
		}
	}

	public override void upgrade ()
	{
		lvl++;
		if (lvl > maxLvl)
			lvl = maxLvl;
	}

	public override string ToString ()
	{
		string ret = "Description : " + descr + "\r\n";
		ret += "Type : personnal.\r\n";
		float val = ((float)launcher.maxHp / 100f) * ((float)boostPerLvl * (float)lvl);
		ret += "Actual value : " + (int)val + " hps(" + (boostPerLvl * lvl) + "%).\r\n";
		ret += "Effective time : immediate.\r\n";
		ret += "Cooldown : " + cooldown + "s.\r\n";
		if (lvl < maxLvl) {
			val = ((float)launcher.maxHp / 100f) * ((float)boostPerLvl * (float)(lvl + 1));
			ret += "Next lvl : " + (int)val + "hps(" + ((lvl + 1) * boostPerLvl) + "%).\r\n";
		}
		return ret;
	}

	IEnumerator destroyPs(ParticleSystem toDestroy){
		yield return new WaitForSeconds (psTime);
		if (toDestroy != null) {
			GameObject.Destroy(toDestroy.gameObject);
		}
	}
}
