﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BoostCON : ISkill {
	private int boostPerLvl = 5;

	void Awake (){
		descr = "Increases CON.";
	}

	void Update(){
		if (remTime > 0)
			remTime -= Time.deltaTime;
		else
			remTime = 0;
	}

	public override void action (){

	}

	public override void upgrade ()
	{
		lvl++;
		if (lvl > maxLvl)
			lvl = maxLvl;
		if (lvl <= maxLvl && launcher != null)
			launcher.CON += 5;
	}

	public override string ToString ()
	{
		string ret = "Description : " + descr + "\r\n";
		ret += "Type : Passive.\r\n";
		ret += "Actual value : " + boostPerLvl * lvl + " CON.\r\n";
		ret += "Effective time : immediate.\r\n";
		if (lvl < maxLvl)
			ret += "Next lvl : " + (boostPerLvl * (lvl + 1)) + " CON.\r\n";
		return ret;
	}
}
