﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TestSkill : ISkill {
	ParticleSystem psInstance;

	// Use this for initialization
	void Start () {
		descr = "Description de test.";
	}
	
	// Update is called once per frame
	void Update () {
		if (cooldown > 0)
			cooldown -= Time.deltaTime;
		if (cooldown < 0)
			cooldown = 0;
	}

	public override void action ()
	{
		if (cooldown <= 0) {
			if (ps != null) {
				psInstance = (ParticleSystem)Instantiate (ps, Maya.maya.transform.position, Maya.maya.transform.rotation);
				psInstance.transform.SetParent (Maya.maya.transform);
				psInstance.Play ();
			}
			cooldown = 5f;
		}
	}

	public override void upgrade ()
	{
		lvl++;
		if (lvl > maxLvl)
			lvl = maxLvl;
	}

	public override string ToString ()
	{
		return descr;
	}
}
