﻿using UnityEngine;
using System.Collections;

public abstract class ISkill : MonoBehaviour {
	public int lvl;
	public int maxLvl;
	public ParticleSystem ps;
	public float psTime;
	public string descr;
	public GameObject tartget;
	public Sprite image;
	public float cooldown;
	public float remTime;
	public ICharacter launcher;

	public abstract void action();
	public abstract void upgrade();
	public abstract string ToString();
}
