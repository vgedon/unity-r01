﻿using UnityEngine;
using System.Collections;

public abstract class ICharacter : MonoBehaviour {
	public int STR = 10;
	public int AGI = 10;
	public int CON = 10;
	public int ARMOR = 0;
	public int armor_stuff = 0;
	public float hp = 0;
	public int maxHp = 0;
	public int level = 1;
	public string	realName;
	public ICharacter target;

	public int Armor {
		get{
			int v = ARMOR + armor_stuff;
			if (v > 180)
			return 180;
			if (v < 0)
				return 0;
			return v;
		}
	}

	public abstract IEnumerator attacking (ICharacter perso);
	public abstract void takeDamage(float damages, int oponentAGI);
	public abstract void heal (int val);
}
