﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class SkillButton : MonoBehaviour {
	public ISkill skillPrefab;
	public ISkill theSkill;
	private Image butImage;
	private Text butTxt;
	private Button but;
	public ToolTip toolTip;
	private RectTransform rt;
	private bool toolTipAff = false;
	public TalentButton dependence;

	// Use this for initialization
	void Start () {
		butImage = GetComponent<Image> ();
		butTxt = transform.GetComponentInChildren<Text> ();
		but = GetComponent<Button> ();
		if (skillPrefab != null) {
			theSkill = (ISkill)Instantiate (skillPrefab);
			theSkill.transform.SetParent(transform);
		}
		rt = GetComponent<RectTransform> ();
		DontDestroyOnLoad (this);
	}

	// Update is called once per frame
	void Update () {
		if (theSkill == null && skillPrefab != null) {
			theSkill = (ISkill)Instantiate (skillPrefab);
			theSkill.transform.SetParent(transform);
		}
		if (theSkill != null) {
			if (butImage != null)
				butImage.sprite = theSkill.image;
			if (butTxt != null){
				if (theSkill.remTime > 0)
					butTxt.text = Math.Round(theSkill.remTime, 1).ToString();
				else
					butTxt.text = "";
			}
			if (theSkill.remTime > 0 && but != null){
				but.interactable = false;
				if (butImage != null)
					butImage.color = Color.grey;
			}
			else if (but != null){
				but.interactable = true;
				if (butImage != null)
					butImage.color = Color.white;
			}
		}
		if (theSkill == null) {
			if (butImage != null)
				butImage.color = Color.black;
			if (but != null)
				but.interactable = false;
			if (butTxt != null)
				butTxt.text = "";
		}
		if (toolTipAff)
			ShowToolTip ();
	}

	public void skillAction(){
		if (theSkill != null)
			theSkill.action ();
	}


	public void ShowToolTip(){
		toolTipAff = true;
		if (toolTip != null && toolTipAff && theSkill != null) {
			toolTip.gameObject.SetActive(true);
			Vector3 pos = transform.position;
			pos.x += (toolTip.rt.rect.width / 2) + (rt.rect.width / 2);
			toolTip.transform.position = pos;
			toolTip.objectName.text = theSkill.name.Replace("(Clone)", "") + "(lvl " + theSkill.lvl + ")";
			toolTip.objectDescr.text = theSkill.ToString();
		}
	}

	public void HideToolTip(){
		toolTipAff = false;
		if (toolTip != null)
			toolTip.gameObject.SetActive (false);
	}

	public void resetDependence(){
		if (dependence != null && theSkill != null) {
			theSkill.transform.SetParent(dependence.transform);
			skillPrefab = null;
			theSkill = null;
			dependence = null;
		}
	}
}
