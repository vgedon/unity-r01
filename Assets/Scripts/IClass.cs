﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IClass : MonoBehaviour {

	public ISkill[] lvl1;
	public ISkill[] lvl2;
	public ISkill[] lvl3;
	public ISkill[] lvl4;
	public ISkill[] lvl5;
	public string name;
	public ImagePosition img;
	public string description;


	public int default_CON = 20;
	public int default_AGI = 20;
	public int default_STR = 20;

}
