﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour {
	public List<Zombi> prefabs = new List<Zombi>();
	private Zombi actual = null;
	private bool init = false;
	private bool spawnLaunched = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (actual == null) {
			if (!init){
				StartCoroutine(spawning(0));
				init = true;
				spawnLaunched = true;
			}
			else if (!spawnLaunched){
				StartCoroutine(spawning(Random.Range(10, 20)));
				spawnLaunched = true;
			}

		}
	}

	IEnumerator spawning(int time){
		yield return new WaitForSeconds (time);
		if (prefabs != null && prefabs.Count > 0 && actual == null) {
			actual = (Zombi)Instantiate(prefabs[Random.Range(0, prefabs.Count)], transform.position, transform.rotation);
			actual.setLvl(Maya.maya.level);
			spawnLaunched = false;
		}
	}
}
