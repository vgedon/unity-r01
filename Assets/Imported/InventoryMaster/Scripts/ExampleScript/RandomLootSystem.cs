﻿using UnityEngine;
using System.Collections;

public class RandomLootSystem : MonoBehaviour
{

    public int amountOfLoot = 1;
    static ItemDataBaseList inventoryItemList;

    int counter = 0;

	// Use this for initialization
    void Start()
    {

        inventoryItemList = (ItemDataBaseList)Resources.Load("ItemDatabase");

        while (counter < amountOfLoot)
        {
            counter++;

            int randomNumber = Random.Range(1, inventoryItemList.itemList.Count - 1);

            if (inventoryItemList.itemList[randomNumber].itemModel == null)
                counter--;
            else
            {
                GameObject randomLootItem = (GameObject)Instantiate(inventoryItemList.itemList[randomNumber].itemModel);
                PickUpItem item = randomLootItem.AddComponent<PickUpItem>();
				item.item = inventoryItemList.itemList[randomNumber].getCopy();
				randomLootItem.transform.position = transform.position;

				float baseDmg = (GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Maya>().level / 5f);
				if (item.item.itemType == ItemType.Weapon) {
					baseDmg += ((ItemAttribute)item.item.itemAttributes [0]).attributeValue;
					item.item.itemDesc = "Damage: " + baseDmg.ToString () + "\nSpeed: " + ((ItemAttribute)item.item.itemAttributes [1]).attributeValue.ToString ();
					((ItemAttribute)item.item.itemAttributes [0]).attributeValue = baseDmg;
				} else {
					float baseArmor = baseDmg + ((ItemAttribute)item.item.itemAttributes [0]).attributeValue;
					item.item.itemDesc = "Armor: " + baseArmor.ToString ();
					((ItemAttribute)item.item.itemAttributes [0]).attributeValue = baseArmor;
				}
            }
        }

    }

}
