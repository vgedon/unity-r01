﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PlayerInventory : MonoBehaviour
{
	public GameObject inventory;
	public GameObject characterSystem;
	private Inventory mainInventory;
	private Inventory characterSystemInventory;
	private Tooltip toolTip;

	private InputManager inputManagerDatabase;

	public GameObject HPMANACanvas;

	int normalSize = 3;

	public void OnEnable ()
	{
		Inventory.ItemEquip += EquipWeapon;
		Inventory.UnEquipItem += UnEquipWeapon;
	}

	public void OnDisable ()
	{
		Inventory.UnEquipItem -= UnEquipWeapon;
		Inventory.ItemEquip -= EquipWeapon;
	}

	void EquipWeapon (Item item)
	{
		if (item.itemType == ItemType.Weapon) {
			//add the weapon if you unequip the weapon
			Maya maya = GetComponent<Maya> ();
			maya.SetWeapon (item.itemModel);
		}
	}

	void UnEquipWeapon (Item item)
	{
		if (item.itemType == ItemType.Weapon) {
			//delete the weapon if you unequip the weapon
			Destroy (GameObject.FindGameObjectWithTag ("WeaponAnchor").transform.GetChild (0).gameObject);
		}
	}

	void changeInventorySize (int size)
	{
		dropTheRestItems (size);

		if (mainInventory == null)
			mainInventory = inventory.GetComponent<Inventory> ();
		if (size == 3) {
			mainInventory.width = 3;
			mainInventory.height = 1;
			mainInventory.updateSlotAmount ();
			mainInventory.adjustInventorySize ();
		}
		if (size == 6) {
			mainInventory.width = 3;
			mainInventory.height = 2;
			mainInventory.updateSlotAmount ();
			mainInventory.adjustInventorySize ();
		} else if (size == 12) {
			mainInventory.width = 4;
			mainInventory.height = 3;
			mainInventory.updateSlotAmount ();
			mainInventory.adjustInventorySize ();
		} else if (size == 16) {
			mainInventory.width = 4;
			mainInventory.height = 4;
			mainInventory.updateSlotAmount ();
			mainInventory.adjustInventorySize ();
		} else if (size == 24) {
			mainInventory.width = 6;
			mainInventory.height = 4;
			mainInventory.updateSlotAmount ();
			mainInventory.adjustInventorySize ();
		}
	}

	void dropTheRestItems (int size)
	{
		if (size < mainInventory.ItemsInInventory.Count) {
			for (int i = size; i < mainInventory.ItemsInInventory.Count; i++) {
				GameObject dropItem = (GameObject)Instantiate (mainInventory.ItemsInInventory [i].itemModel);
				dropItem.AddComponent<PickUpItem> ();
				dropItem.GetComponent<PickUpItem> ().item = mainInventory.ItemsInInventory [i];
				dropItem.transform.localPosition = GameObject.FindGameObjectWithTag ("Player").transform.localPosition;
			}
		}
	}

	void Start ()
	{
		if (inputManagerDatabase == null)
			inputManagerDatabase = (InputManager)Resources.Load ("InputManager");


		if (GameObject.FindGameObjectWithTag ("Tooltip") != null)
			toolTip = GameObject.FindGameObjectWithTag ("Tooltip").GetComponent<Tooltip> ();
		if (inventory != null)
			mainInventory = inventory.GetComponent<Inventory> ();
		if (characterSystem != null)
			characterSystemInventory = characterSystem.GetComponent<Inventory> ();
	}

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (inputManagerDatabase.EscapeKeyCode)) {
			if (toolTip != null)
				toolTip.deactivateTooltip ();
			characterSystemInventory.closeInventory ();
			mainInventory.closeInventory ();
		}
		if (Input.GetKeyDown (inputManagerDatabase.CharacterSystemKeyCode)) {
			if (!characterSystem.activeSelf) {
				characterSystemInventory.openInventory ();
			} else {
				if (toolTip != null)
					toolTip.deactivateTooltip ();
				characterSystemInventory.closeInventory ();
			}
		}

		if (Input.GetKeyDown (inputManagerDatabase.InventoryKeyCode)) {
			if (!inventory.activeSelf) {
				mainInventory.openInventory ();
			} else {
				if (toolTip != null)
					toolTip.deactivateTooltip ();
				mainInventory.closeInventory ();
			}
		}


	}

}
